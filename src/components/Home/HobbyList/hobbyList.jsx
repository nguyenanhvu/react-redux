import React from "react";
import PropTypes from "prop-types";
import "./hobbyList.css";
import { useDispatch, useSelector } from "react-redux";
import { setActiveHobby } from "../../../actions/hobbyAction";

HobbyList.propTypes = {
  hobbyList: PropTypes.array,
};

HobbyList.defaultProps = {
  hobbyList: [],
};

function HobbyList(props) {
  const { hobbyList } = props;
  const activeId = useSelector((state) => state.hobby.activeId);
  const dispatch = useDispatch();

  const handleClickItem = (id) => {
    const action = setActiveHobby(id);
    dispatch(action);
  };

  return (
    <ul className="hobby-list">
      {hobbyList.map((item) => (
        <li
          key={item.id}
          className={item.id === activeId ? "active" : ""}
          onClick={() => handleClickItem(item.id)}
        >
          {item.title}
        </li>
      ))}
    </ul>
  );
}

export default HobbyList;
