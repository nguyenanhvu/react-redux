const initialState = {
  list: [],
  activeId: null,
};

const hobbyReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_HOBBY":
      const newLists = [...state.list];
      newLists.unshift(action.payload);
      return {
        ...state,
        list: newLists,
      };

    case "SET_ACTIVE_HOBBY":
      const newId = action.payload;
      return { ...state, activeId: newId };
    default:
      return state;
  }
};

export default hobbyReducer;
