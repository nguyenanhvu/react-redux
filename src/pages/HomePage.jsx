import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewHobby } from "../actions/hobbyAction";
import HobbyList from "../components/Home/HobbyList/hobbyList";

const randomId = () => {
  return 100000 + Math.trunc(Math.random() * 900000);
};

function HomePage() {
  const hobbyList = useSelector((state) => state.hobby.list);

  const dispatch = useDispatch();

  const handleAddHobbyClick = () => {
    const newId = randomId();
    const newHobby = {
      id: newId,
      title: `Hobby ${newId}`,
    };

    dispatch(addNewHobby(newHobby));
  };

  return (
    <div className="home-page">
      <h1>Home Page</h1>
      <button onClick={handleAddHobbyClick}>Random hobby</button>
      <HobbyList hobbyList={hobbyList} />
    </div>
  );
}

export default HomePage;
